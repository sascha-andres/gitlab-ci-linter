module gitlab.com/orobardet/gitlab-ci-linter

go 1.13

require (
	github.com/fatih/color v1.5.0
	github.com/go-ini/ini v1.29.2
	github.com/mattn/go-colorable v0.0.9
	github.com/mattn/go-isatty v0.0.3
	github.com/urfave/cli v1.20.0
	golang.org/x/sys v0.0.0-20171017063910-8dbc5d05d6ed
)
